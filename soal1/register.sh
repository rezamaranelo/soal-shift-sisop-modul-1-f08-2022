#!/bin/bash

log () {
	`echo "$(date +'%D %T') $1" >> log.txt`
}

#cek apakah ada directory users
if ! [[ -d "users" ]]; then
	mkdir users && touch users/user.txt
fi

#username
echo "Halo! Mohon regis terlebih dahulu"
echo "Silahkan masukkan username Anda"

while true
do
read -p "Username: " username

if grep -Fq "$username" users/user.txt; then
	log "REGISTER: ERROR User already exists"
	echo "REGISTER: ERROR User already exists"
else
	break
fi
done

#password
echo "Silahkan masukkan password Anda"

while true
do
	read -s -p "Password: " password
	
	if [[ "$username" != "$password" ]]; then
		if [[ ${#password} -ge 8 ]]; then
			if [[ $password =~ [a-z] && $password =~ [A-Z] ]]; then
				if [[ $password =~ [0-9] ]]; then
					log "REGISTER: INFO User $username registered succesfully"
					echo -e "\nREGISTER: INFO User $username registered succesfully"
					break
				else
					echo -e "\nPassword harus alphanumeric!"
				fi
			else
				echo -e "\nPassword harus memiliki minimal 1 huruf kapital dan 1 huruf kecil"
			fi
		else
			echo -e "\nPassword harus memiliki minimal 8 karakter!"
		fi
	else
		echo -e "\nPassword tidak boleh sama dengan username!"
	fi
done

echo $username $password >> users/user.txt

