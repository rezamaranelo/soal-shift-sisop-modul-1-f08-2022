#!/bin/bash

log () {
	`echo "$(date +'%D %T') $1" >> log.txt`
}

echo "Silahkan login."

while true
do
read -p "Username: " username

#login
check_username=$(awk '$1 ~ /'$username'/ {print $1}' users/user.txt)
if [[ "$username" == "$check_username" ]]; then
	check_password=$(awk '$1 ~ /'$username'/ {print $2}' users/user.txt)
	read -s -p "Password: " password
	if [[ "$password" == "$check_password" ]]; then
		log "LOGIN: INFO User $username logged in"
		echo -e "\nLOGIN: INFO User $username logged in"
		break
	else
		log "LOGIN: ERROR Failed login attempt on user $username"
		echo -e "\nLOGIN: ERROR Failed login attempt on user $username"
	fi
else
	echo "Username tidak terdaftar! Coba lagi"
fi
done

#setelah login, milih command
echo -e "\nSelamat datang $username!"
echo -e "Silahkan memilih perintah yang akan dilakukan.\n"

while true
do
	echo "COMMAND LIST"
	echo "1. dl N (N = Jumlah gambar yang akan didownload)"
	echo "2. att"

	read -p "Command yang dipilih: " command

	if [[ "$command" == "dl" ]]; then
		read N
		nfolder=`echo "$(date +'%F_')$username"`
		#cek zip
		if ! [[ -f "$nfolder.zip" ]]; then
			mkdir $nfolder
			i=1
		else
			unzip $nfolder.zip
			rm -r $folder.zip
			i=`ls $nfolder | wc -l`
			N=$N+$i
		fi
		
		#download
		until ! [[ $i -le $N ]]; do
			if [[ $i -lt 10 ]]; then
				wget https://loremflickr.com/320/240 -O $nfolder/PIC_0$i.jpg
			else
				wget https://loremflickr.com/320/240 -O $nfolder/PIC_$i.jpg
			fi
			((i++))
		done
		
		#zip folder
		zip -P $password -r $nfolder.zip $nfolder
		rm -r $nfolder
		
		#lanjut atau tidak
		echo "Lanjut? (Y/N)"
		read cntnue
		if [[ "$cntnue" == "Y" ]]; then 
			continue
		elif [[ "$cntnue" == "N" ]]; then 
			break
		else 
			exit
		fi
		
	elif [[ "$command" == "att" ]]; then
		awk '/'$username'/ && /LOGIN/ {print;++n} END {print "Jumlah percobaan login sebanyak", n, "kali."}' log.txt
		
		#lanjut atau tidak
		echo "Lanjut? (Y/N)"
		read cntnue
		if [[ "$cntnue" == "Y" ]]; then 
			continue
		elif [[ "$cntnue" == "N" ]]; then 
			break
		else 
			exit
		fi
	else exit
	fi
done


