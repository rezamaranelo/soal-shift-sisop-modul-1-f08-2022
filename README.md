**<br>1. Reza Maranelo Alifiansyah (5025201071)**
**<br>2. Natya Madya Marciola (5025201238)**
**<br>3. Khuria Khusna (5025201053)**

## Soal 1
Pada suatu hari, Han dan teman-temannya diberikan tugas untuk mencari foto. Namun,
karena laptop teman-temannya rusak ternyata tidak bisa dipakai karena rusak, Han dengan
senang hati memperbolehkan teman-temannya untuk meminjam laptopnya. Untuk
mempermudah pekerjaan mereka, Han membuat sebuah program.

a. Han membuat sistem register pada script register.sh dan setiap user yang berhasil
didaftarkan disimpan di dalam file ./users/user.txt. Han juga membuat sistem login
yang dibuat di script main.sh

b. Demi menjaga keamanan, input password pada login dan register harus
tertutup/hidden dan password yang didaftarkan memiliki kriteria sebagai berikut
- Minimal 8 karakter
- Memiliki minimal 1 huruf kapital dan 1 huruf kecil
- Alphanumeric
- Tidak boleh sama dengan username

c. Setiap percobaan login dan register akan tercatat pada log.txt dengan format :
MM/DD/YY hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi
yang dilakukan user.
- Ketika mencoba register dengan username yang sudah terdaftar, maka
message pada log adalah REGISTER: ERROR User already exists
- Ketika percobaan register berhasil, maka message pada log adalah REGISTER:
INFO User USERNAME registered successfully
- Ketika user mencoba login namun passwordnya salah, maka message pada
log adalah LOGIN: ERROR Failed login attempt on user USERNAME
- Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User
USERNAME logged in

d. Setelah login, user dapat mengetikkan 2 command dengan dokumentasi sebagai
berikut :
- dl N ( N = Jumlah gambar yang akan didownload), untuk mendownload gambar dari https://loremflickr.com/320/240 dengan
jumlah sesuai dengan yang diinputkan oleh user. Hasil download akan
dimasukkan ke dalam folder dengan format nama
YYYY-MM-DD_USERNAME. Gambar-gambar yang didownload juga memiliki
format nama PIC_XX, dengan nomor yang berurutan (contoh : PIC_01,
PIC_02, dst. ). Setelah berhasil didownload semua, folder akan otomatis di
zip dengan format nama yang sama dengan folder dan dipassword sesuai
dengan password user tersebut. Apabila sudah terdapat file zip dengan
nama yang sama, maka file zip yang sudah ada di unzip terlebih dahulu,
barulah mulai ditambahkan gambar yang baru, kemudian folder di zip
kembali dengan password sesuai dengan user.
- att, untuk menghitung jumlah percobaan login baik yang berhasil maupun tidak dari
user yang sedang login saat ini

### Penyelesaian Soal
**Nomor 1a**

Untuk membuat sistem register pada script `register.sh` dan sistem login pada script `main.sh`, kita ketikkan pada terminal sebagai berikut. **Note:** *Text editor* yang digunakan pada soal ini adalah gedit.

```ruby
$ gedit register.sh
$ gedit main.sh
```
Setiap username dan password yang berhasil didaftarkan akan disimpan di dalam file ./users/user.txt.
```ruby
echo $username $password >> users/user.txt
```

**Nomor 1b**

Input password pada login dan register harus tertutup/hidden. Kita bisa mengetikkannya sebagai berikut
```ruby
read -s -p "Password: " password
```
> Fungsi dari `-s` adalah agar input yang kita ketikkan tidak muncul pada terminal. Sedangkan `-p` digunakan untuk menampilkan string prompt sebelum mengeksekusi read.

Untuk mengecek kriteria password
```ruby
if [[ "$username" != "$password" ]]; then
	if [[ ${#password} -ge 8 ]]; then
		if [[ $password =~ [a-z] && $password =~ [A-Z] ]]; then
			if [[ $password =~ [0-9] ]]; then
				log "REGISTER: INFO User $username registered succesfully"
				echo -e "\nREGISTER: INFO User $username registered succesfully"
				break
			else
				echo -e "\nPassword harus alphanumeric!"
			fi
		else
		    echo -e "\nPassword harus memiliki minimal 1 huruf kapital dan 1 huruf kecil"
		fi
	else
		echo -e "\nPassword harus memiliki minimal 8 karakter!"
	fi
else
	echo -e "\nPassword tidak boleh sama dengan username!"
fi
done
```
>`${#password}` digunakan untuk menghitung banyaknya karakter pada string. <br>`$password =~ [a-z] && $password =~ [A-Z]` mengecek apakah string mengandung huruf kecil dan huruf kapital.<br>`$password =~ [0-9]` mengecek apakah string mengandung angka

**Algoritma code:**
* Pertama, password akan dicek apakah sama dengan username. Jika sama, maka akan dioutputkan seperti yang tertera pada code di atas . Jika tidak, maka akan dilakukan pengecekan kembali untuk if yang kedua.
* Pada pengecekan if yang selanjutnya adalah mengecek apakah passowrd lebih dari 8 karakter. Jika tidak, maka outputnya adalah seperti yang tertera di atas. Jika sudah memenuhi, maka dilanjutkan dengan pengecekan if berikutnya.
* Pengecekan if selanjutnya adalah mengecek apakah password mengandung huruf kapital dan huruf kecil. Jika tidak, maka outputnya seperti yang tertera. Jika ya, maka akan dilakukan pengecekan if yang terakhir.
* Pengecekan yang terakhir adalah memastikan bahwa password merupakan alphanumeric. Jika Tidak, maka output seperti yang tertera. Jika ya, maka akan tercatat pada log.txt menggunakan fungsi yang sudah dibuat tadi.

**Nomor 1c**

Format untuk mencatat percobaan login dan register adalah MM/DD/YY hh:mm:ss MESSAGE. Untuk mempermudah kita dalam pembuatannya, maka buatlah sebuah fungsi log dengan format di atas.

```ruby
log () {
    `echo "$(date +'%D %T') $1" >> log.txt`
}
```
> `%D` dan `%T` merupakan format code. <br>`%D` (format tanggal) sama artinya dengan `%m/%d/%y` <br>`%T` (format waktu) sama artinya dengan `%H:%M:%S`

Karena message pada log akan berbeda bergantung pada aksi yang dilakukan user, maka kita perlu membuat sebuah *conditional statements* dengan ketentuan yang sudah ditetapkan pada soal.

Pada bagian ini, awk digunakan untuk membantu proses membandingkan username dan password.

```ruby
check_username=$(awk '$1 ~ /'$username'/ {print $1}' users/user.txt)
if [[ "$username" == "$check_username" ]]; then
	check_password=$(awk '$1 ~ /'$username'/ {print $2}' users/user.txt)
	read -s -p "Password: " password
	if [[ "$password" == "$check_password" ]]; then
		log "LOGIN: INFO User $username logged in"
		echo -e "\nLOGIN: INFO User $username logged in"
		break
	else
		log "LOGIN: ERROR Failed login attempt on user $username"
		echo -e "\nLOGIN: ERROR Failed login attempt on user $username"
	fi
else
	echo "Username tidak terdaftar! Coba lagi"
fi
done
```

**Algoritma code:**
* Pertama, akan dilakukan pengecekan username yang diinput oleh user. Untuk pengecekan username akan melibatkan penggunaan dari awk. Jika username yang diinputkan terdaftar dan tercatat pada file user.txt, maka perintah bisa dilanjutkan dengan memasukkan password dari username tersebut. Jika tidak, maka akan ada pemberitahuan bahwa username tersebut tidak terdaftar.
* Tidak jauh berbeda dengan tahap pertama, pengecekan password yang diinputkan oleh user juga dilakukan. Sama seperti tadi, pada tahap ini juga akan melibatkan penggunaan awk.
* Ketika username dan password yang diinputkan sudah sesuai dengan apa yang tercatat, maka user akan berhasil login dan percobaan loginnya akan tercatat pada file log.txt

**Nomor 1d**

```ruby
if [[ "$command" == "dl" ]]; then
    read N
	nfolder=`echo "$(date +'%F_')$username"`
	#cek zip
	if ! [[ -f "$nfolder.zip" ]]; then
		mkdir $nfolder
		i=1
	else
		unzip $nfolder.zip
		rm -r $folder.zip
		i=`ls $nfolder | wc -l`
		N=$N+$i
	fi
	
	#download
	until ! [[ $i -le $N ]]; do
		if [[ $i -lt 10 ]]; then
			wget https://loremflickr.com/320/240 -O $nfolder/PIC_0$i.jpg
		else
			wget https://loremflickr.com/320/240 -O $nfolder/PIC_$i.jpg
		fi
		((i++))
	done
	
	#zip folder
	zip -P $password -r $nfolder.zip $nfolder
	rm -r $nfolder
	
	#lanjut atau tidak
	echo "Lanjut? (Y/N)"
	read cntnue
	if [[ "$cntnue" == "Y" ]]; then 
		continue
	elif [[ "$cntnue" == "N" ]]; then 
		break
	else 
		exit
	fi
	
elif [[ "$command" == "att" ]]; then
	awk '/'$username'/ && /LOGIN/ {print;++n} END {print "Jumlah percobaan login sebanyak", n, "kali."}' log.txt
	
	#lanjut atau tidak
	echo "Lanjut? (Y/N)"
	read cntnue
	if [[ "$cntnue" == "Y" ]]; then 
		continue
	elif [[ "$cntnue" == "N" ]]; then 
		break
	else 
		exit
	fi
else exit
fi
```
> `ls $nfolder | wc -l` menghitung jumlah file yang ada dalam suatu folder. <br>`-f "$nfolder.zip"` mengecek apakah suatu file ada atau tidak. <br>Fungsi `-P` pada `-P $password ` gampangnya adalah untuk memberikan keamanan. Sehingga pada saat dieksekusi user perlu memasukkan password terlebih dahulu.

**Algoritma Code:**
* Terdapat 2 command yang bisa dilakukan. Pertama adalah `dl N` untuk mendownload gambar dengan jumlah yang diinputkan oleh user. Kedua adalah `att` untuk menghitung jumlah percobaan login. 
* Jika yang dipilih adalah `dl` maka selanjutnya user akan menginputkan sebuah integer. Karena setiap user yang login akan memiliki nama foldernya masing-masing sesuatu format pada soal, maka akan dilakukan pengecekan apakah folder dengan nama tersebut sudah ada atau tidak. Jika tidak, maka program akan membuat folder baru. Jika ada, maka folder tersebut (dalam format zip) akan diunzip terlebih dahulu, dengan tujuan agar gambar baru yang akan didownload dapat ditambahkan ke folder tersebut. Selanjutnya gambar akan didownload sebanyak yang user inputkan. Lalu folder tadi akan diubah menjadi format zip. Dan user akan diberikan pilihan apakah akan melanjutkan atau tidak.
* Jika yang dipilih adalah `att`, maka akan dilakukan perhitungan menggunakan program awk. Lalu user akan diberikan pilihan untuk melanjutkan atau tidak.

**Kendala Pengerjaan**
- Masih belum terbiasa menggunakan command grep dan penggunaan awk. <br>
Berikut ini merupakan beberapa error yang terjadi pada saat pengerjaan
![error1](img/1.jpg)
![error2](img/2.jpg)
![error1](img/3.jpg)

## Soal 2
Pada tanggal 22 Januari 2022, website https://daffa.info di hack oleh seseorang yang tidak bertanggung jawab. Sehingga hari sabtu yang seharusnya hari libur menjadi berantakan. Dapos langsung membuka log website dan menemukan banyak request yang berbahaya. Bantulah Dapos untuk membaca log website https://daffa.info Buatlah sebuah script awk bernama "soal2_forensic_dapos.sh" untuk melaksanakan tugas-tugas berikut:
### Penyelesaian Soal
**A**
Buat folder terlebih dahulu bernama forensic_log_website_daffainfo_log.
```sh
mkdir -p forensic_log_website_daffainfo_log
```
Kita buat folder untuk menampung file hasil forensic

Kita split file log agar lebih mudah dibaca
```sh
splittedres=$(awk -F'["]' '{ for(i=2;i<=NF;i++) { if($i == ":") continue; printf "%s ",  $i} printf "\n"}' log_website_daffainfo.log | awk '(NR>1)')
```
Dipisahkan berdasar delimiter simbol ":", lalu awk akan assign secara otomatis

**B**
Dikarenakan serangan yang diluncurkan ke website https://daffa.info sangat banyak, Dapos ingin tahu berapa rata-rata request per jam yang dikirimkan penyerang ke website. Kemudian masukkan jumlah rata-ratanya ke dalam sebuah file bernama ratarata.txt ke dalam folder yang sudah dibuat sebelumnya.
```sh
echo "$splittedres" | awk -F'[:]' '{jumlah[$2]++} END {for (hadir in jumlah) { jam++; total+=0+jumlah[hadir] } printf "Rata-rata serangan adalah sebanyak %.3f requests per jam\n", 0+total/jam}' > forensic_log_website_daffainfo_log/ratarata.txt
```
Jika variabel 2 masih ada data, masukan terus ke array dan dibagi dengan jumlah total jam

**C**
Sepertinya penyerang ini menggunakan banyak IP saat melakukan serangan ke website https://daffa.info, Dapos ingin menampilkan IP yang paling banyak melakukan request ke server dan tampilkan berapa banyak request yang dikirimkan dengan IP tersebut. Masukkan outputnya kedalam file baru bernama result.txt kedalam folder yang sudah dibuat sebelumnya.
```sh
awk -F '"' '{print $2}' $localog | sort | uniq -c | sort | awk 'END{printf "IP yang paling banyak mengakses server adalah: %s sebanyak %d requests\n\n" ,$2,$1}'> forensic_log_website_daffainfo_log/result.txt
```
Baca variabel ke 2 lalu tuliskan ip akses terbanyak

**D**
Beberapa request ada yang menggunakan user-agent ada yang tidak. Dari banyaknya request, berapa banyak requests yang menggunakan user-agent curl?
Kemudian masukkan berapa banyak requestnya kedalam file bernama result.txt yang telah dibuat sebelumnya.
```sh
echo -e "Ada $(grep -c 'curl' $localog) requests yang menggunakan curl sebagai user-agent\n" >> forensic_log_website_daffainfo_log/result.txt
```
Baca log kemudian cari keyword "curl" dan tulis jumlahnya

**E**
Pada jam 2 pagi pada tanggal 22 terdapat serangan pada website, Dapos ingin mencari tahu daftar IP yang mengakses website pada jam tersebut. Kemudian masukkan daftar IP tersebut kedalam file bernama result.txt yang telah dibuat sebelumnya.
```sh
echo "$splittedres" | awk -F'[/ ]' '$4~ /2022:02/ {printf "%s Jam 2 pagi\n", $1}' >> forensic_log_website_daffainfo_log/result.txt
```
Cari line yang mengandung string 2022:02 dan simpan jumlah kemudian print
## Soal 3
Ubay sangat suka dengan komputernya. Suatu saat komputernya crash secara tiba-tiba :(.
Tentu saja Ubay menggunakan linux. Akhirnya Ubay pergi ke tukang servis untuk
memperbaiki laptopnya. Setelah selesai servis, ternyata biaya servis sangatlah mahal
sehingga ia harus menggunakan dana kenakalannya untuk membayar biaya servis tersebut.
Menurut Mas Tukang Servis, laptop Ubay overload sehingga mengakibatkan crash pada
laptopnya. Karena tidak ingin hal serupa terulang, Ubay meminta kalian untuk membuat
suatu program monitoring resource yang tersedia pada komputer.<br>
Buatlah program monitoring resource pada komputer kalian. Cukup monitoring ram dan
monitoring size suatu directory. Untuk ram gunakan command `free -m`. Untuk disk gunakan
command `du -sh <target_path>`. Catat semua metrics yang didapatkan dari hasil `free -m`.
Untuk hasil `du -sh <target_path>` catat size dari path directory tersebut. Untuk target_path
yang akan dimonitor adalah /home/{user}/.

a. Masukkan semua metrics ke dalam suatu file log bernama metrics_{YmdHms}.log.
{YmdHms} adalah waktu disaat file script bash kalian dijalankan. Misal dijalankan
pada 2022-01-31 15:00:00, maka file log yang akan tergenerate adalah
metrics_20220131150000.log.

b. Script untuk mencatat metrics diatas diharapkan dapat berjalan otomatis pada setiap
menit

c. Kemudian, buat satu script untuk membuat agregasi file log ke satuan jam. Script
agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file
agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap
metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis.
Berikut contoh nama file hasil agregasi metrics_agg_2022013115.log dengan format
metrics_agg_{YmdH}.log

d. Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user
pemilik file.

### Penyelesaian Soal
**Nomor 3a**

Untuk membuat format file log menjadi sesuai dengan yang diminta soal, kita bisa membuat format *date* dan *target path* seperti berikut.
```ruby
date=`echo "$(date +'%Y%m%d%H%M%S')"`
target_path=`echo "/home/$(whoami)"`
```
> command `whoami` digunakan untuk mengetahui user

Kemudian untuk penggunaannya nanti kurang lebih kita bisa mengetikkan
```ruby
$target_path/log/metrics_$date.log
```

Lalu, berikut merupakan kode untuk memasukkan semua metrics ke dalam file log.
```ruby
ram=$(free -m | awk 'BEGIN {init=2} {for(i=init; i<=NF; i++) {if(NR > 1) printf("%d,", $i)}}')
disk=$(du -sh $target_path | awk '{printf("%s,%s", $2, $1)}')

if ! [[ -d "/home/$(whoami)/log" ]]; then
	mkdir /home/$(whoami)/log
fi

echo -e "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size\n$ram$disk" >> $target_path/log/metrics_$date.log
```

Sebelum membahas lebih lanjut, mari kita lihat output dari perintah free -m pada saat dijalankan di terminal.
![](img/3_1.jpg)

Sedangkan untuk du -sh <target_path> akan menghasilkan output sebagai berikut
![](img/3_2.jpg)

Berdasarkan output tersebut, kita bisa menggunakan awk untuk mendapatkan data-data yang nantikan akan kita masukkan pada file sesuai dengan format yang sudah ditentukan pada soal.
```ruby
ram=$(free -m | awk 'BEGIN {init=2} {for(i=init; i<=NF; i++) {if(NR > 1) printf("%d,", $i)}}')
disk=$(du -sh $target_path | awk '{printf("%s,%s", $2, $1)}')
```

Karena file log tadi akan diletakkan pada direktori log, maka kita akan mengecek apakah direktori log sudah ada atau tidak 
```ruby
if ! [[ -d "/home/$(whoami)/log" ]]; then
	mkdir /home/$(whoami)/log
fi
```

Lalu setelah mendapatkan data-data yang diperlukan kita bisa memasukkannya ke dalam file `metrics_{YmdHms}`
```ruby
echo -e "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size\n$ram$disk" >> $target_path/log/metrics_$date.log
```

**Nomor 3b**

Agar script untuk mencatat metrics bisa berjalan secara otomatis setiap menit, kita bisa menggunakan cron. Pada terminal, ketikkan
```ruby
crontab -e
```
Nantinya editor crontab akan terbuka, lalu kita bisa mengisi perintah crontab sesuai dengan parameternya. Dalam hal ini, kita akan membuat program `minute_log.sh` berjalan secara otomatis setiap menit, maka kita bisa menggunakan `* * * * *`. Karena dalam hal ini program `minute_log.sh` berada pada /home/natya/Modul1, maka path yang akan terbentuk adalah `/home/natya/Modul1/minute_log.sh`. Berikut merupakan implementasinya pada editor crontab.
```ruby
* * * * * home/natya/Modul1/minute_log.sh
```

**Nomor 3d**

Untuk membuat file log agar hanya dapat dibaca oleh user pemilik file, maka kita bisa menggunakan `chmod` sebagai berikut.
```ruby
chmod 400 $target_path/log/metrics_$date.log
```
> 400 adalah representasi dari permission yang diberikan. Angka pertama melambangkan permission untuk user, angka kedua untuk group, dan angka ke tiga untuk Others.<br>
`4` permission untuk read only sedangkan `0` permission untuk none

**Kendala Pengerjaan**
- Masih sedikit kesulitan dalam mengoptimalkan penggunaan awk.


