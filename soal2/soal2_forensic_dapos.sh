#!/bin/bash
# shellcheck disable=SC2164
cd "$(pwd)"
localog="log_website_daffainfo.log"

#A Create Directory
mkdir -p forensic_log_website_daffainfo_log

#Splitting log file for easier analysis for some question
splittedres=$(awk -F'["]' '{ for(i=2;i<=NF;i++) { if($i == ":") continue; printf "%s ",  $i} printf "\n"}' log_website_daffainfo.log | awk '(NR>1)')

#B Attack per hour
echo "$splittedres" | awk -F'[:]' '{absen[$2]++} END {for (hadir in absen) { jam++; total+=0+absen[hadir] } printf "Rata-rata serangan adalah sebanyak %.3f requests per jam\n", 0+total/jam}' > forensic_log_website_daffainfo_log/ratarata.txt

#C Request per IP and the most IP with most request
awk -F '"' '{print $2}' $localog | sort | uniq -c | sort | awk 'END{printf "IP yang paling banyak mengakses server adalah: %s sebanyak %d requests\n\n" ,$2,$1}'> forensic_log_website_daffainfo_log/result.txt

#D Curl and non Curl
echo -e "Ada $(grep -c 'curl' $localog) requests yang menggunakan curl sebagai user-agent\n" >> forensic_log_website_daffainfo_log/result.txt

#E IP that access during date 22 and 0200 time
echo "$splittedres" | awk -F'[/ ]' '$4~ /2022:02/ {printf "%s Jam 2 pagi\n", $1}' >> forensic_log_website_daffainfo_log/result.txt






